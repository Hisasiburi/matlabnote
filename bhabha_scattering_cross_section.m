syms c s;

uUp1 = [1;0;1;0];
uDn1 = [0;1;0;-1];
vUp2 = [1;0;-1;0];
vDn2 = [0;1;0;1];
uUp3 = [c;s;c;s];
uDn3 = [-s;c;s;-c];
vUp4 = [c;s;-c;-s];
vDn4 = [s;-c;s;-c];
u1 = [uUp1, uDn1];
v2 = [vUp2, vDn2];
u3 = [uUp3, uDn3];
v4 = [vUp4, vDn4];

gma0 = [1 0 0 0;0 1 0 0;0 0 -1 0;0 0 0 -1];
gma1 = [0 0 0 1;0 0 1 0;0 -1 0 0;-1 0 0 0];
gma2 = [0 0 0 -1i;0 0 1i 0;0 1i 0 0;-1i 0 0 0];
gma3 = [0 0 1 0;0 0 0 -1;-1 0 0 0;0 1 0 0];
gma = cat(4, gma0, gma1, gma2, gma3);
metric = [1 0 0 0;0 -1 0 0;0 0 -1 0;0 0 0 -1];

disp("v2u1")
for i = 1:2
    for j = 1:2
        disp("start");
        for k = 1:4
            val = v2(:,i)'*gma0*gma(:,:,k)*u1(:,j);
            disp(val);
        end
        disp("end")
    end
end
disp("u3v4")
for i = 1:2
    for j = 1:2
        disp("start")
        for k = 1:4
            val = u3(:,i)'*gma0*metric*gma(:,:,k)*v4(:,j);
            disp(val);
        end
        disp("end")
    end
end
disp("v2v4")
for i = 1:2
    for j = 1:2
        disp("start")
        for k = 1:4
            disp(v2(:,i)'*gma0*gma(:,:,k)*v4(:,j));
        end
        disp("end")
    end
end
disp("u3u1")
for i = 1:2
    for j = 1:2
        disp("start")
        for k = 1:4
            disp(u3(:,i)'*gma0*metric*gma(:,:,k)*u1(:,j));
        end
        disp("end")
    end
end