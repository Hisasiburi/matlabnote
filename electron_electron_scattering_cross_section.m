syms c s;

uUp1 = [1;0;1;0];
uDn1 = [0;1;0;-1];
uUp2 = [0;-1;0;-1];
uDn2 = [-1;0;1;0];
uUp3 = [c;s;c;s];
uDn3 = [-s;c;s;-c];
uUp4 = [s;-c;s;-c];
uDn4 = [-c;-s;c;s];
u1 = [uUp1, uDn1];
u2 = [uUp2, uDn2];
u3 = [uUp3, uDn3];
u4 = [uUp4, uDn4];

gma0 = [1 0 0 0;0 1 0 0;0 0 -1 0;0 0 0 -1];
gma1 = [0 0 0 1;0 0 1 0;0 -1 0 0;-1 0 0 0];
gma2 = [0 0 0 -1i;0 0 1i 0;0 1i 0 0;-1i 0 0 0];
gma3 = [0 0 1 0;0 0 0 -1;-1 0 0 0;0 1 0 0];
gma = cat(4, gma0, gma1, gma2, gma3);

disp("u3u1")
for i = 1:2
    for j = 1:2
        disp("start")
        for k = 1:4
            disp(u3(:,i)'*gma0*gma(:,:,k)*u1(:,j));
        end
        disp("end")
    end
end
disp("u4u2")
for i = 1:2
    for j = 1:2
        disp("start")
        for k = 1:4
            disp(u4(:,i)'*gma0*gma(:,:,k)*u2(:,j));
        end
        disp("end")
    end
end
disp("u4u1")
for i = 1:2
    for j = 1:2
        disp("start")
        for k = 1:4
            disp(u4(:,i)'*gma0*gma(:,:,k)*u1(:,j));
        end
        disp("end")
    end
end
disp("u3u2")
for i = 1:2
    for j = 1:2
        disp("start")
        for k = 1:4
            disp(u3(:,i)'*gma0*gma(:,:,k)*u2(:,j));
        end
        disp("end")
    end
end